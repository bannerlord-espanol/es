**Traducción de Mount and Blade II: Bannerlord**

Traducción popular al Español.


**Como ayudar a traducir.**

1º Registrate en la plataforma

2º Si ya estas en este repositorio y registrado ve a la carpeta **es**, alli están todos los documentos.


3º Entras en cualquiera de los documentos y le das a Edit. Te saldra un aviso par hacer un Fork,cuando aceptes podras editar.

4º Vé a la linea que no esté traducida y entre las "eltextoeningles" cambias el texto por su traduccion al español, por ejemplo:

    <string id="2wclahIJ" text="Throwing" />

Sería:

    <string id="2wclahIJ" text="Arrojando" />

5º Cuando hayas acabado o quieres enviarlo para que quede guardado le das al botón verde que pone "Commit changes"

6º Te aparecerá otra pantalla por si quieres poner una descripción o dar el motivo para que se traduzca, será bien recibido. Pulsa en el botón verde "Submit merge request" para enviarlo.

7º Cambiará la pantalla y querra decir que se ha enviado para su aprovación. Si ocurriera algo se mandarian mensajes que puedes ver en la pestaña izquierda "Merge Requests".

8º Si ubiera un error o quisieran que arreglaras algo que has puesto mal te aparecerá el mensaje en "Merge Requests". Entras en el mensaje y verás las sugerencias que han hecho. 

* Para cambiarla ve a la pestaña superior "Changes".
* 	Pulsar el boton en forma de lápiz que indica "Edit file"
* 	Hacer los cambios que ha pedido
* 	Repetir los pasos 5, 6 y 7


**Reglas de traducción básicas:**

-Se tienen que usar las tildes.

-Las traducciones tienen que ser lo más acertadas posibles.

-Todos los textos a traducir tienen el siguiente formato:

     <string id="1dLZLQsY" text="Donate to Troops{newline}You will not be able to undo this action." />
     
Todos los cambios se han de hacer dentro del " ". **NO** se tiene que traducir el texto en { } ni [] y **NUNCA** tiene que haber un punto despues de "" final.